## Summary


## Issue
Clone this Repository to create a new deployment for your application or set of applications. The Framework has its roots in the Ansible space but it uses a lot of other tools and concepts.

## Agenda
### Inventory / Environments

The inventory diretory should work for ansible and has 2 parts . The first part is a host list and found in the root of the inventory directory. The secound part are the settings and configurations of the applications.

- [ ] Inventory
- [ ] group_vars
- [ ] host_vars

### Ansible Collection / Role
These are the logic part of the deployment there is a default strcture build in an ansible role template with usefull learnings from a Coutinious Delivery perspective.

- [ ] Collections
- [ ] Roles

## Notes

## Worklog
Detailed Steps to take for the first deployment 

<details>

- [ ] Create Merge Request 
- [ ] Update Repository inventory/all.yml for your needs

    ```
    # Example Structure for a 
servicename:
   children:
        servicename-production:
            hosts:
                hostname.prod.servicename.site.region:
                    ansible_host:
        servicename-canary:
            hosts:
                hostname.canary.servicename.site.region:
                    ansible_host:
                hostname2.canary.servicename.site.region:
                    ansible_host:
        servicename-review:
            hosts:
                hostname.review.servicename.site.region:
                    ansible_connection: local
    ```

- [ ] Select Reviewer
- [ ] Mark as Ready
- [ ] Merge to Master
</details>