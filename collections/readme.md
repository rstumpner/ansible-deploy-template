# Collections and Roles in one Requirement file
- The Place for Ansible Collections and Roles
- a  better Way is change the requirements.yml to get the dependencies via git or Ansible Galaxy

## manual fetching ansible collections and roles
`ansible-galaxy collection install -r collections/requirements.yml`

## Pipeline Variable
CICD_ANSIBLE_COLLECTION_PATH: 'collections/requirements.yml'