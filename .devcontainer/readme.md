## Ansible Deployment Sidecar
Using the technology of conatiners and virtual machines to deploy this scenario on local machine or in the ci/cd pipeline

### Use vscode devcontainer
#### install a ready to use environment for manual testing
- install vscode (https://code.visualstudio.com/)
- install remote development extension (https://github.com/Microsoft/vscode-remote-release)
- start devcontainer in vscode

<!-- 
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install requests
- sudo /usr/local/py-utils/venvs/ansible-core/bin/python3 -m pip install docker
- execute molecule test 
- execute molecule converge 
-->

### Execute prepared Playbooks in local container (CI/CD Action local)
- ansible-playbook --connection=local playbooks/ansible-playbooks.yml -vvvv # optional with sodo rights -bkK

### Execute prepared Playbooks in local container with predefined environments (CI/CD Action local)
- ansible-playbook -i environments/local/review.yml playbooks/stage-release-issue-templates.yml -vvvv # optional with sodo rights -bkK -bkK

### install ansible roles for the deployment
- ansible-galaxy install -r roles/requirements.yml

### install ansible collections for deployment
- ansible-galaxy install -r collections/requirements.yml

### run deployment stage validation syntax
- export ANSIBLE_DEFAULT_INVENTORY="inventory"
- ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY:?} all-services.yml --syntax-check -vvvv

### run deployment stage validation tasks
- ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY:?} all-services.yml --list-tasks

### run deployment stage validation inventory
- ansible-inventory -i ${ANSIBLE_DEFAULT_INVENTORY:?} --list

### run deployment service oriented stage
- ansible-playbook -i ${ANSIBLE_DEFAULT_INVENTORY:?} all-services.yml --tags build # optional for a deployment password --vault-password-file /tmp/vault-password