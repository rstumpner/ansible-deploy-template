# Ansible Repository for deploying Services in an Automated Way

This Ansible Repository is a Deployment Framework for Services in a modern Way. It tries to be a Template for Testing Ansible Roles or deploy services with automation in mind.There is an CI/CD Pipeline included witch runs automaticaly in Gitlab , but if you have other ways it should be very easy to adopt it to other deployment solutions.


## Feature Integrated CI/CD Pipeline 
Stages with Ansible tags for execution included:
- validate
- build
- preflight
- deploy-test
- deploy-canary
- deploy-production


#### Validate:
Validates the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --syntax-check -vvvv`

#### Build:
Builds the configuration files from the Deployment with `ansible-playbook -i inventory/all.yml all-services.yml --tags build --vault-password-file /tmp/vault-password`

#### Inventory:
Some words about the Inventory

#### Environments:
Some words about the Environments that are tested


## Feature Artifacts

Artifacts are collected on all stages . On the different stages there may be different artifacts like config files on the build stage and some reports on the deployment stage.

Artifacts path:

| path | description | Ansible tag|
|--- | --- |--- | 
| build | some artifacts from the build process (configs/packages)| [build]|
| test | Files or Reports from the Testing stage | [build] |
| state | some artifacts from the deployment process (diffs/reports) | [deploy] |
| docs | some artifacts from the generated documentation | [build,docs]|


## Feature sidecar devcontainers

This is meaned to be a repository for deploying a service in a continious way and the place were all the information (docs) deployment steps (ansible collection and roles with stages) resists and also have playbooks and scripts for hotfixes and single tasks. These tasks can be executed in a ci/cd pipeline to do a continious and versioned way of deployment. But in case of an incident it is possible to execute those playbooks local from your laptop. With devcontainers (https://containers.dev) you have a tool to setup your environment for deployment in minutes. Just follow the instrutions in the .devconatiner directory.



## Feature sidecar ansible execution environment (ansible-ee) [state exeriment]
In ansible there is an feature called ansible execution environment . With that it is possible to build an execution environment with all collections and roles build in a container to execute your deployment.

## Build the ansible execution envirionment
- cd meta
- ansible-builder build -f execution-environment.yml

## build the container 
docker build -f context/Dockerfile -t ansible-execution-env:latest context

## release the container 
docker push $CI_REGISTRY_IMAGE/$CICD_SIDECAR_DEPLOYMENT_IMAGE

## run a playbook in the container (verify)
ansible-navigator run all-services.yml --eei ansible-execution-env:latest --pp never -b -m stdout


## Notes
- https://ansible.readthedocs.io/projects/builder


License:
    MIT / BSD

Author Information:
roland@stumpner.at
