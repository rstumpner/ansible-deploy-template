# Ansible Execution Environment (experiment)
Directory of the Ansible Execution Environment

## update rqirement files (if links do not work)
- copy .devcontainer/requirements.txt meta/
- copy collections/requirements.yml meta/

## Build the ansible execution envirionment
ansible-builder build -f ansible-ee/execution-environment.yml

## build the container 
docker build -f context/Dockerfile -t ansible-execution-env:latest context

## run a playbook in the container
ansible-navigator run all-services.yml --eei ansible-execution-env:latest --pp never -b -m stdout


## Notes
- https://ansible.readthedocs.io/projects/builder